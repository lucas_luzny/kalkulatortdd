﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            button1_Click(null, null);
            button2_Click(null, null);
            button3_Click(null, null);
            button4_Click(null, null);
            button5_Click(null, null);
            button6_Click(null, null);
            button7_Click(null, null);
            button8_Click(null, null);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var kalk = new kalkulator.Kalkulator();

                if (kalk.wartosc != "0")
                    throw new Exception();

                if (kalk.sysDanych != kalkulator.Kalkulator.SysDanych.Dec)
                    throw new Exception();

                if (kalk.typDanych != kalkulator.Kalkulator.TypDanych.Qword)
                    throw new Exception();

                button1.BackColor = Color.Green;
            }
            catch (Exception)
            {
                button1.BackColor = Color.Red;
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                var kalk = new kalkulator.Kalkulator();

                kalk.wartosc = "4";

                if (kalk.wartosc != "4")
                    throw new Exception();

                button2.BackColor = Color.Green;
            }
            catch (Exception)
            {
                button2.BackColor = Color.Red;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                var kalk = new kalkulator.Kalkulator();

                if (kalk.dodaj("2", "2") != "4")
                    throw new Exception();

                if (kalk.odejmij("2", "2") != "0")
                    throw new Exception();

                if (kalk.pomnoz("2", "2") != "4")
                    throw new Exception();

                if (kalk.podziel("2", "2") != "1")
                    throw new Exception();


                button3.BackColor = Color.Green;

            }
            catch (Exception)
            {
                button3.BackColor = Color.Red;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                var kalk = new kalkulator.Kalkulator();

                kalk.wartosc = "4";

                kalk.kasuj();

                if (kalk.wartosc != "0")
                    throw new Exception();

                button4.BackColor = Color.Green;
            }
            catch (Exception)
            {
                button4.BackColor = Color.Red;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                var kalk = new kalkulator.Kalkulator();

                kalk.wartosc = "123456";

                kalk.cofaj();

                if (kalk.wartosc != "12345")
                    throw new Exception();

                button5.BackColor = Color.Green;
            }
            catch (Exception)
            {
                button5.BackColor = Color.Red;
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                var kalk = new kalkulator.Kalkulator();

                kalk.wartosc = "5";

                kalk.plusMinus();

                if (kalk.wartosc != "-5")
                    throw new Exception();

                button6.BackColor = Color.Green;
            }
            catch (Exception)
            {
                button6.BackColor = Color.Red;
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                var kalk = new kalkulator.Kalkulator();

                kalk.wartosc = "8";

                kalk.konwertuj(kalkulator.Kalkulator.SysDanych.Bin);

                if (kalk.wartosc != "1000")
                    throw new Exception();

                kalk.wartosc = "8";

                kalk.konwertuj(kalkulator.Kalkulator.SysDanych.Oct);

                if (kalk.wartosc != "10")
                    throw new Exception();

                kalk.wartosc = "8";

                kalk.konwertuj(kalkulator.Kalkulator.SysDanych.Dec);

                if (kalk.wartosc != "8")
                    throw new Exception();

                kalk.wartosc = "15";

                kalk.konwertuj(kalkulator.Kalkulator.SysDanych.Hex);

                if (kalk.wartosc != "f")
                    throw new Exception();

                button7.BackColor = Color.Green;
            }
            catch (Exception)
            {
                button7.BackColor = Color.Red;
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            try
            {
                var kalk = new kalkulator.Kalkulator();

                kalk.wartosc = "1230ABC";

                kalk.wprowadz(kalkulator.Kalkulator.SysDanych.Dec);

                if (kalk.wartosc != "1230")
                    throw new Exception();

                kalk.wartosc = "1230ABC";

                kalk.wprowadz(kalkulator.Kalkulator.SysDanych.Bin);

                if (kalk.wartosc != "10")
                    throw new Exception();

                kalk.wartosc = "123809ABC";

                kalk.wprowadz(kalkulator.Kalkulator.SysDanych.Oct);

                if (kalk.wartosc != "12380")
                    throw new Exception();


                kalk.wartosc = "123809ABC";

                kalk.wprowadz(kalkulator.Kalkulator.SysDanych.Hex);

                if (kalk.wartosc != "123809ABC")
                    throw new Exception();

                button8.BackColor = Color.Green;
            }
            catch (Exception)
            {
                button8.BackColor = Color.Red;
            }
        }
    }
}
