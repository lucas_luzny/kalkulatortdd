﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace kalkulator
{
    public class Kalkulator
    {
        public enum SysDanych { Hex, Dec, Oct, Bin };
        public enum TypDanych { Qword, Dword, Word, Bajt };

        public Kalkulator()
        {
            wartosc = "0";
            wartoscDoDzialania = "";
            sysDanych = kalkulator.Kalkulator.SysDanych.Dec;
            typDanych = kalkulator.Kalkulator.TypDanych.Qword;
        }
        public string wartosc { get; set; }

        public string wartoscDoDzialania { get; set; }

        public SysDanych sysDanych { get; set; }
    
        public TypDanych typDanych { get; set; }

        public string dodaj(string a, string b)
        {
            int a2 = Int32.Parse(a);
            int b2 = Int32.Parse(b);
            int rezultat = a2 + b2;
            return rezultat.ToString();
        }

        public string odejmij(string a, string b)
        {
            int a2 = Int32.Parse(a);
            int b2 = Int32.Parse(b);
            int rezultat = a2 - b2;
            return rezultat.ToString();
        }

        public string pomnoz(string a, string b)
        {
            int a2 = Int32.Parse(a);
            int b2 = Int32.Parse(b);
            int rezultat = a2 * b2;
            return rezultat.ToString();
        }

        public string podziel(string a, string b)
        {
            int a2 = Int32.Parse(a);
            int b2 = Int32.Parse(b);
            int rezultat = a2 / b2;
            return rezultat.ToString();
        }

        public void kasuj()
        {
            wartosc = "0";
            wartoscDoDzialania = "";
        }

        public void cofaj()
        {
            if (wartosc.Length == 1)
                wartosc = "0";
            else
                wartosc = wartosc.Substring(0, wartosc.Length - 1);
        }

        public void plusMinus()
        {
            int nowaWart = Int32.Parse(wartosc) * -1;
            wartosc = nowaWart.ToString(); 
        }

        public void konwertuj(SysDanych sysDanych)
        {
            int value = Int32.Parse(wartosc);

            switch (sysDanych)
            {
                case SysDanych.Hex:
                    wartosc = Convert.ToString(value, 16);
                    break;

                case SysDanych.Dec:
                    wartosc = Convert.ToString(value, 10);
                    break;

                case SysDanych.Oct:
                    wartosc = Convert.ToString(value, 8);
                    break;

                case SysDanych.Bin:
                    wartosc = Convert.ToString(value, 2);
                    break;
            }
        }

        public void wprowadz(SysDanych sysDanych)
        {
            switch (sysDanych)
            {
                case SysDanych.Hex:
                    wartosc = Regex.Replace(wartosc, "^([a-fA-F0-9]{2}\\s+)+", "");
                    break;

                case SysDanych.Dec:
                    wartosc = Regex.Replace(wartosc, "[^0-9.]", "");
                    break;

                case SysDanych.Oct:
                    wartosc = Regex.Replace(wartosc, "[^0-8.]", "");
                    break;

                case SysDanych.Bin:
                    wartosc = Regex.Replace(wartosc, "[^0-1.]", "");
                    break;
            }
        }
    }
}
