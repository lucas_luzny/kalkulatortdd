﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace kalkulator
{
    public partial class Form1 : Form
    {
        public Kalkulator kalkulator;
        public bool isOperation = false;
        public string operation;
        public Button[] buttons;
        public RadioButton[] rButtons;
        public Button[] hexDisabled;
        public Button[] decDisabled;
        public Button[] octDisabled;
        public Button[] binDisabled;

        public Form1()
        {
            InitializeComponent();
            Load += new EventHandler(Form1_Load);
        }

        public void Form1_Load(object sender, EventArgs e)
        {
            this.kalkulator = new Kalkulator();
            mainTextBox.Text = this.kalkulator.wartosc;
            Button[] buttons =  { buttonRoot, buttonPercent, button1x, buttonDot, button0, button1, button2, button3, button4, button5, button6, button7, button8, button9,
                buttonA, buttonB, buttonC, buttonD, buttonE, buttonF, buttonPlus, buttonMinus, buttonMulti, buttonDivide, buttonEqual, buttonBackspace, buttonDeleteCE, buttonDeleteC, buttonPM };
            this.buttons = buttons;
            RadioButton[] rButtons = { systemHex, systemDec, systemOct, systemBin, RadioWordOword, RadioWordDword, RadioWordWord, RadioWordByte };
            this.rButtons = rButtons;
            Button[] btnsHex = { buttonRoot, buttonPercent, button1x, buttonDot };
            this.hexDisabled = btnsHex;
            Button[] btnsDec = { buttonRoot, buttonPercent, button1x, buttonDot, buttonA, buttonB, buttonC, buttonD, buttonE, buttonF };
            this.decDisabled = btnsDec;
            Button[] btnsOct = { buttonRoot, buttonPercent, button1x, buttonDot, button8, button9, buttonA, buttonB, buttonC, buttonD, buttonE, buttonF };
            this.octDisabled = btnsOct;
            Button[] btnsBin = { buttonRoot, buttonPercent, button1x, buttonDot, button2, button3, button4, button5, button6, button7, button8, button9, buttonA, buttonB, buttonC, buttonD, buttonE, buttonF };
            this.binDisabled = btnsBin;

            handleDataSystemChange(Kalkulator.SysDanych.Hex);
        }

        public string getCheckedRadioName()
        {
            foreach (Control control in this.Controls)
            {
                if (typeof(RadioButton) == control.GetType())
                {
                    RadioButton rb = (RadioButton)control;

                    if (rb.Checked)
                    {
                        return control.Name;
                    }
                }
            }
            return null;
        }

        public void addToMainTextBox(char c)
        {
            if (isOperation)
            {
                mainTextBox.Text = c.ToString();
            }
            else
            {
                mainTextBox.Text += c;
            }
            this.isOperation = false;
        }

        public void buttons_Click(object sender, EventArgs e)
        {
            var b = (Button)sender;
            this.addToMainTextBox(char.Parse(b.Text));
        }

        public void buttonBackspace_Click(object sender, EventArgs e)
        {
            kalkulator.cofaj();
            mainTextBox.Text = kalkulator.wartosc;
        }

        public void buttonEqual_Click(object sender, EventArgs e)
        {
            isOperation = false;
            switch (this.operation)
            {
                case "addition":
                    kalkulator.wartosc = this.kalkulator.dodaj(this.kalkulator.wartoscDoDzialania, this.kalkulator.wartosc);
                    break;
                case "substraction":
                    kalkulator.wartosc = this.kalkulator.odejmij(this.kalkulator.wartoscDoDzialania, this.kalkulator.wartosc);
                    break;
                case "multiplication":
                    kalkulator.wartosc = this.kalkulator.pomnoz(this.kalkulator.wartoscDoDzialania, this.kalkulator.wartosc);
                    break;
                case "division":
                    kalkulator.wartosc = this.kalkulator.podziel(this.kalkulator.wartoscDoDzialania, this.kalkulator.wartosc);
                    break;

            }

            mainTextBox.Text = this.kalkulator.wartosc;
            this.kalkulator.wartosc = "0";
            this.kalkulator.wartoscDoDzialania = "";
            operationsTextbox.Text = "";
        }

        public void buttonPlus_Click(object sender, EventArgs e)
        {
            isOperation = true;
            kalkulator.wartoscDoDzialania += Int64.Parse(mainTextBox.Text);
            operationsTextbox.Text += mainTextBox.Text + " + ";
            mainTextBox.Text = kalkulator.wartosc;
            operation = "addition";
        }

        public void buttonMinus_Click(object sender, EventArgs e)
        {
            isOperation = true;
            kalkulator.wartoscDoDzialania += Int64.Parse(mainTextBox.Text);
            operationsTextbox.Text += mainTextBox.Text + " - ";
            mainTextBox.Text = kalkulator.wartosc;
            operation = "substraction";
        }

        public void buttonMulti_Click(object sender, EventArgs e)
        {
            isOperation = true;
            kalkulator.wartoscDoDzialania += Int64.Parse(mainTextBox.Text);
            operationsTextbox.Text += mainTextBox.Text + " * ";
            mainTextBox.Text = kalkulator.wartosc;
            operation = "multiplication";
        }

        public void buttonDivide_Click(object sender, EventArgs e)
        {
            isOperation = true;
            kalkulator.wartoscDoDzialania += Int64.Parse(mainTextBox.Text);
            operationsTextbox.Text += mainTextBox.Text + " / ";
            mainTextBox.Text = kalkulator.wartosc;
            operation = "division";
        }

        public void buttonDeleteCE_Click(object sender, EventArgs e)
        {
            kalkulator.kasuj();
            mainTextBox.Text = kalkulator.wartosc;
            operationsTextbox.Text = kalkulator.wartoscDoDzialania;
        }

        public void buttonDeleteC_Click(object sender, EventArgs e)
        {
            kalkulator.kasuj();
            mainTextBox.Text = kalkulator.wartosc;
            operationsTextbox.Text = kalkulator.wartoscDoDzialania;
        }

        public void mainTextBox_TextChanged_1(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(mainTextBox.Text))
            {
                if (mainTextBox.Text.Length > 1 && mainTextBox.Text.StartsWith("0"))
                {
                    mainTextBox.Text = mainTextBox.Text.Substring(1, mainTextBox.Text.Length - 1);
                }
            }
            kalkulator.wartosc = mainTextBox.Text;
        }

        public void buttonPM_Click(object sender, EventArgs e)
        {
            if (!isOperation)
            {
                kalkulator.wartosc = mainTextBox.Text;
                kalkulator.plusMinus();
                mainTextBox.Text = kalkulator.wartosc;
            }
        }

        public void systemHex_CheckedChanged(object sender, EventArgs e)
        {
            handleDataSystemChange(Kalkulator.SysDanych.Hex);
        }

        public void systemDec_CheckedChanged(object sender, EventArgs e)
        {
            handleDataSystemChange(Kalkulator.SysDanych.Dec);      
        }

        public void systemOct_CheckedChanged(object sender, EventArgs e)
        {
            handleDataSystemChange(Kalkulator.SysDanych.Oct);
        }

        public void systemBin_CheckedChanged(object sender, EventArgs e)
        {
            handleDataSystemChange(Kalkulator.SysDanych.Bin);
        }

        public void handleDataSystemChange(Kalkulator.SysDanych dataSystem)
        {
            kalkulator.sysDanych = dataSystem;

            foreach (var button in this.Controls.OfType<Button>())
            {
                button.Enabled = true;
            }

            switch (dataSystem)
            {
                case Kalkulator.SysDanych.Bin:
                    foreach (var butt in binDisabled)
                    {
                        butt.Enabled = false;
                    }
                    break;
                case Kalkulator.SysDanych.Dec:
                    foreach (var butt in decDisabled)
                    {
                        butt.Enabled = false;
                    }
                    break;
                case Kalkulator.SysDanych.Hex:
                    foreach (var butt in hexDisabled)
                    {
                        butt.Enabled = false;
                    }
                    break;
                case Kalkulator.SysDanych.Oct:
                    foreach (var butt in octDisabled)
                    {
                        butt.Enabled = false;
                    }
                    break;
            }

        }
    }
}
