﻿namespace kalkulator
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.RadioWordWord = new System.Windows.Forms.RadioButton();
            this.RadioWordOword = new System.Windows.Forms.RadioButton();
            this.RadioWordDword = new System.Windows.Forms.RadioButton();
            this.RadioWordByte = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.systemOct = new System.Windows.Forms.RadioButton();
            this.systemHex = new System.Windows.Forms.RadioButton();
            this.systemDec = new System.Windows.Forms.RadioButton();
            this.systemBin = new System.Windows.Forms.RadioButton();
            this.buttonEmpty = new System.Windows.Forms.Button();
            this.buttonMod = new System.Windows.Forms.Button();
            this.buttonA = new System.Windows.Forms.Button();
            this.buttonMC = new System.Windows.Forms.Button();
            this.buttonMminus = new System.Windows.Forms.Button();
            this.buttonMplus = new System.Windows.Forms.Button();
            this.buttonMS = new System.Windows.Forms.Button();
            this.buttonMR = new System.Windows.Forms.Button();
            this.buttonRoot = new System.Windows.Forms.Button();
            this.buttonPM = new System.Windows.Forms.Button();
            this.buttonDeleteC = new System.Windows.Forms.Button();
            this.buttonDeleteCE = new System.Windows.Forms.Button();
            this.buttonBackspace = new System.Windows.Forms.Button();
            this.buttonB = new System.Windows.Forms.Button();
            this.buttonRP = new System.Windows.Forms.Button();
            this.buttonLP = new System.Windows.Forms.Button();
            this.buttonPercent = new System.Windows.Forms.Button();
            this.buttonDivide = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.buttonC = new System.Windows.Forms.Button();
            this.buttonRoR = new System.Windows.Forms.Button();
            this.buttonRoL = new System.Windows.Forms.Button();
            this.buttonPlus = new System.Windows.Forms.Button();
            this.buttonDot = new System.Windows.Forms.Button();
            this.button0 = new System.Windows.Forms.Button();
            this.buttonF = new System.Windows.Forms.Button();
            this.buttonAnd = new System.Windows.Forms.Button();
            this.buttonNot = new System.Windows.Forms.Button();
            this.buttonEqual = new System.Windows.Forms.Button();
            this.buttonMinus = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonE = new System.Windows.Forms.Button();
            this.buttonRsh = new System.Windows.Forms.Button();
            this.buttonLsh = new System.Windows.Forms.Button();
            this.button1x = new System.Windows.Forms.Button();
            this.buttonMulti = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.buttonD = new System.Windows.Forms.Button();
            this.buttonXor = new System.Windows.Forms.Button();
            this.buttonOr = new System.Windows.Forms.Button();
            this.LabelBinary = new System.Windows.Forms.Label();
            this.operationsTextbox = new System.Windows.Forms.TextBox();
            this.mainTextBox = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RadioWordWord);
            this.groupBox1.Controls.Add(this.RadioWordOword);
            this.groupBox1.Controls.Add(this.RadioWordDword);
            this.groupBox1.Controls.Add(this.RadioWordByte);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Location = new System.Drawing.Point(12, 256);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(84, 111);
            this.groupBox1.TabIndex = 51;
            this.groupBox1.TabStop = false;
            // 
            // RadioWordWord
            // 
            this.RadioWordWord.AutoSize = true;
            this.RadioWordWord.Location = new System.Drawing.Point(10, 58);
            this.RadioWordWord.Name = "RadioWordWord";
            this.RadioWordWord.Size = new System.Drawing.Size(51, 17);
            this.RadioWordWord.TabIndex = 16;
            this.RadioWordWord.Text = "Word";
            this.RadioWordWord.UseVisualStyleBackColor = true;
            // 
            // RadioWordOword
            // 
            this.RadioWordOword.AutoSize = true;
            this.RadioWordOword.Checked = true;
            this.RadioWordOword.Location = new System.Drawing.Point(10, 12);
            this.RadioWordOword.Name = "RadioWordOword";
            this.RadioWordOword.Size = new System.Drawing.Size(56, 17);
            this.RadioWordOword.TabIndex = 15;
            this.RadioWordOword.TabStop = true;
            this.RadioWordOword.Text = "Qword";
            this.RadioWordOword.UseVisualStyleBackColor = true;
            // 
            // RadioWordDword
            // 
            this.RadioWordDword.AutoSize = true;
            this.RadioWordDword.Location = new System.Drawing.Point(9, 35);
            this.RadioWordDword.Name = "RadioWordDword";
            this.RadioWordDword.Size = new System.Drawing.Size(56, 17);
            this.RadioWordDword.TabIndex = 17;
            this.RadioWordDword.Text = "Dword";
            this.RadioWordDword.UseVisualStyleBackColor = true;
            // 
            // RadioWordByte
            // 
            this.RadioWordByte.AutoSize = true;
            this.RadioWordByte.Location = new System.Drawing.Point(10, 81);
            this.RadioWordByte.Name = "RadioWordByte";
            this.RadioWordByte.Size = new System.Drawing.Size(46, 17);
            this.RadioWordByte.TabIndex = 18;
            this.RadioWordByte.Text = "Byte";
            this.RadioWordByte.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.systemOct);
            this.groupBox2.Controls.Add(this.systemHex);
            this.groupBox2.Controls.Add(this.systemDec);
            this.groupBox2.Controls.Add(this.systemBin);
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox2.Location = new System.Drawing.Point(12, 133);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(84, 117);
            this.groupBox2.TabIndex = 52;
            this.groupBox2.TabStop = false;
            // 
            // systemOct
            // 
            this.systemOct.AutoSize = true;
            this.systemOct.Location = new System.Drawing.Point(10, 58);
            this.systemOct.Name = "systemOct";
            this.systemOct.Size = new System.Drawing.Size(42, 17);
            this.systemOct.TabIndex = 16;
            this.systemOct.Text = "Oct";
            this.systemOct.UseVisualStyleBackColor = true;
            this.systemOct.CheckedChanged += new System.EventHandler(this.systemOct_CheckedChanged);
            // 
            // systemHex
            // 
            this.systemHex.AutoSize = true;
            this.systemHex.Checked = true;
            this.systemHex.Location = new System.Drawing.Point(10, 12);
            this.systemHex.Name = "systemHex";
            this.systemHex.Size = new System.Drawing.Size(44, 17);
            this.systemHex.TabIndex = 15;
            this.systemHex.TabStop = true;
            this.systemHex.Text = "Hex";
            this.systemHex.UseVisualStyleBackColor = true;
            this.systemHex.CheckedChanged += new System.EventHandler(this.systemHex_CheckedChanged);
            // 
            // systemDec
            // 
            this.systemDec.AutoSize = true;
            this.systemDec.Location = new System.Drawing.Point(9, 35);
            this.systemDec.Name = "systemDec";
            this.systemDec.Size = new System.Drawing.Size(45, 17);
            this.systemDec.TabIndex = 17;
            this.systemDec.Text = "Dec";
            this.systemDec.UseVisualStyleBackColor = true;
            this.systemDec.CheckedChanged += new System.EventHandler(this.systemDec_CheckedChanged);
            // 
            // systemBin
            // 
            this.systemBin.AutoSize = true;
            this.systemBin.Location = new System.Drawing.Point(10, 81);
            this.systemBin.Name = "systemBin";
            this.systemBin.Size = new System.Drawing.Size(40, 17);
            this.systemBin.TabIndex = 18;
            this.systemBin.Text = "Bin";
            this.systemBin.UseVisualStyleBackColor = true;
            this.systemBin.CheckedChanged += new System.EventHandler(this.systemBin_CheckedChanged);
            // 
            // buttonEmpty
            // 
            this.buttonEmpty.Location = new System.Drawing.Point(103, 139);
            this.buttonEmpty.Name = "buttonEmpty";
            this.buttonEmpty.Size = new System.Drawing.Size(41, 33);
            this.buttonEmpty.TabIndex = 54;
            this.buttonEmpty.UseVisualStyleBackColor = true;
            // 
            // buttonMod
            // 
            this.buttonMod.Location = new System.Drawing.Point(150, 139);
            this.buttonMod.Name = "buttonMod";
            this.buttonMod.Size = new System.Drawing.Size(41, 33);
            this.buttonMod.TabIndex = 55;
            this.buttonMod.Text = "Mod";
            this.buttonMod.UseVisualStyleBackColor = true;
            // 
            // buttonA
            // 
            this.buttonA.Location = new System.Drawing.Point(197, 139);
            this.buttonA.Name = "buttonA";
            this.buttonA.Size = new System.Drawing.Size(41, 33);
            this.buttonA.TabIndex = 56;
            this.buttonA.Text = "A";
            this.buttonA.UseVisualStyleBackColor = true;
            this.buttonA.Click += new System.EventHandler(this.buttons_Click);
            // 
            // buttonMC
            // 
            this.buttonMC.Location = new System.Drawing.Point(244, 139);
            this.buttonMC.Name = "buttonMC";
            this.buttonMC.Size = new System.Drawing.Size(41, 33);
            this.buttonMC.TabIndex = 60;
            this.buttonMC.Text = "MC";
            this.buttonMC.UseVisualStyleBackColor = true;
            // 
            // buttonMminus
            // 
            this.buttonMminus.Location = new System.Drawing.Point(430, 139);
            this.buttonMminus.Name = "buttonMminus";
            this.buttonMminus.Size = new System.Drawing.Size(41, 33);
            this.buttonMminus.TabIndex = 64;
            this.buttonMminus.Text = "M-";
            this.buttonMminus.UseVisualStyleBackColor = true;
            // 
            // buttonMplus
            // 
            this.buttonMplus.Location = new System.Drawing.Point(383, 139);
            this.buttonMplus.Name = "buttonMplus";
            this.buttonMplus.Size = new System.Drawing.Size(41, 33);
            this.buttonMplus.TabIndex = 63;
            this.buttonMplus.Text = "M+";
            this.buttonMplus.UseVisualStyleBackColor = true;
            // 
            // buttonMS
            // 
            this.buttonMS.Location = new System.Drawing.Point(336, 139);
            this.buttonMS.Name = "buttonMS";
            this.buttonMS.Size = new System.Drawing.Size(41, 33);
            this.buttonMS.TabIndex = 62;
            this.buttonMS.Text = "MS";
            this.buttonMS.UseVisualStyleBackColor = true;
            // 
            // buttonMR
            // 
            this.buttonMR.Location = new System.Drawing.Point(289, 139);
            this.buttonMR.Name = "buttonMR";
            this.buttonMR.Size = new System.Drawing.Size(41, 33);
            this.buttonMR.TabIndex = 61;
            this.buttonMR.Text = "MR";
            this.buttonMR.UseVisualStyleBackColor = true;
            // 
            // buttonRoot
            // 
            this.buttonRoot.Location = new System.Drawing.Point(430, 178);
            this.buttonRoot.Name = "buttonRoot";
            this.buttonRoot.Size = new System.Drawing.Size(41, 33);
            this.buttonRoot.TabIndex = 72;
            this.buttonRoot.Text = "pierwiastek";
            this.buttonRoot.UseVisualStyleBackColor = true;
            // 
            // buttonPM
            // 
            this.buttonPM.Location = new System.Drawing.Point(383, 178);
            this.buttonPM.Name = "buttonPM";
            this.buttonPM.Size = new System.Drawing.Size(41, 33);
            this.buttonPM.TabIndex = 71;
            this.buttonPM.Text = "+ -";
            this.buttonPM.UseVisualStyleBackColor = true;
            this.buttonPM.Click += new System.EventHandler(this.buttonPM_Click);
            // 
            // buttonDeleteC
            // 
            this.buttonDeleteC.Location = new System.Drawing.Point(336, 178);
            this.buttonDeleteC.Name = "buttonDeleteC";
            this.buttonDeleteC.Size = new System.Drawing.Size(41, 33);
            this.buttonDeleteC.TabIndex = 70;
            this.buttonDeleteC.Text = "C";
            this.buttonDeleteC.UseVisualStyleBackColor = true;
            this.buttonDeleteC.Click += new System.EventHandler(this.buttonDeleteC_Click);
            // 
            // buttonDeleteCE
            // 
            this.buttonDeleteCE.Location = new System.Drawing.Point(289, 178);
            this.buttonDeleteCE.Name = "buttonDeleteCE";
            this.buttonDeleteCE.Size = new System.Drawing.Size(41, 33);
            this.buttonDeleteCE.TabIndex = 69;
            this.buttonDeleteCE.Text = "CE";
            this.buttonDeleteCE.UseVisualStyleBackColor = true;
            this.buttonDeleteCE.Click += new System.EventHandler(this.buttonDeleteCE_Click);
            // 
            // buttonBackspace
            // 
            this.buttonBackspace.Location = new System.Drawing.Point(244, 178);
            this.buttonBackspace.Name = "buttonBackspace";
            this.buttonBackspace.Size = new System.Drawing.Size(41, 33);
            this.buttonBackspace.TabIndex = 68;
            this.buttonBackspace.Text = "<--";
            this.buttonBackspace.UseVisualStyleBackColor = true;
            this.buttonBackspace.Click += new System.EventHandler(this.buttonBackspace_Click);
            // 
            // buttonB
            // 
            this.buttonB.Location = new System.Drawing.Point(197, 178);
            this.buttonB.Name = "buttonB";
            this.buttonB.Size = new System.Drawing.Size(41, 33);
            this.buttonB.TabIndex = 67;
            this.buttonB.Text = "B";
            this.buttonB.UseVisualStyleBackColor = true;
            this.buttonB.Click += new System.EventHandler(this.buttons_Click);
            // 
            // buttonRP
            // 
            this.buttonRP.Location = new System.Drawing.Point(150, 178);
            this.buttonRP.Name = "buttonRP";
            this.buttonRP.Size = new System.Drawing.Size(41, 33);
            this.buttonRP.TabIndex = 66;
            this.buttonRP.Text = ")";
            this.buttonRP.UseVisualStyleBackColor = true;
            // 
            // buttonLP
            // 
            this.buttonLP.Location = new System.Drawing.Point(103, 178);
            this.buttonLP.Name = "buttonLP";
            this.buttonLP.Size = new System.Drawing.Size(41, 33);
            this.buttonLP.TabIndex = 65;
            this.buttonLP.Text = "(";
            this.buttonLP.UseVisualStyleBackColor = true;
            // 
            // buttonPercent
            // 
            this.buttonPercent.Location = new System.Drawing.Point(430, 217);
            this.buttonPercent.Name = "buttonPercent";
            this.buttonPercent.Size = new System.Drawing.Size(41, 33);
            this.buttonPercent.TabIndex = 80;
            this.buttonPercent.Text = "%";
            this.buttonPercent.UseVisualStyleBackColor = true;
            // 
            // buttonDivide
            // 
            this.buttonDivide.Location = new System.Drawing.Point(383, 217);
            this.buttonDivide.Name = "buttonDivide";
            this.buttonDivide.Size = new System.Drawing.Size(41, 33);
            this.buttonDivide.TabIndex = 79;
            this.buttonDivide.Text = "/";
            this.buttonDivide.UseVisualStyleBackColor = true;
            this.buttonDivide.Click += new System.EventHandler(this.buttonDivide_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(336, 217);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(41, 33);
            this.button9.TabIndex = 78;
            this.button9.Text = "9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.buttons_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(289, 217);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(41, 33);
            this.button8.TabIndex = 77;
            this.button8.Text = "8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.buttons_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(244, 217);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(41, 33);
            this.button7.TabIndex = 76;
            this.button7.Text = "7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.buttons_Click);
            // 
            // buttonC
            // 
            this.buttonC.Location = new System.Drawing.Point(197, 217);
            this.buttonC.Name = "buttonC";
            this.buttonC.Size = new System.Drawing.Size(41, 33);
            this.buttonC.TabIndex = 75;
            this.buttonC.Text = "C";
            this.buttonC.UseVisualStyleBackColor = true;
            this.buttonC.Click += new System.EventHandler(this.buttons_Click);
            // 
            // buttonRoR
            // 
            this.buttonRoR.Location = new System.Drawing.Point(150, 217);
            this.buttonRoR.Name = "buttonRoR";
            this.buttonRoR.Size = new System.Drawing.Size(41, 33);
            this.buttonRoR.TabIndex = 74;
            this.buttonRoR.Text = "RoR";
            this.buttonRoR.UseVisualStyleBackColor = true;
            // 
            // buttonRoL
            // 
            this.buttonRoL.Location = new System.Drawing.Point(103, 217);
            this.buttonRoL.Name = "buttonRoL";
            this.buttonRoL.Size = new System.Drawing.Size(41, 33);
            this.buttonRoL.TabIndex = 73;
            this.buttonRoL.Text = "RoL";
            this.buttonRoL.UseVisualStyleBackColor = true;
            // 
            // buttonPlus
            // 
            this.buttonPlus.Location = new System.Drawing.Point(382, 334);
            this.buttonPlus.Name = "buttonPlus";
            this.buttonPlus.Size = new System.Drawing.Size(41, 33);
            this.buttonPlus.TabIndex = 103;
            this.buttonPlus.Text = "+";
            this.buttonPlus.UseVisualStyleBackColor = true;
            this.buttonPlus.Click += new System.EventHandler(this.buttonPlus_Click);
            // 
            // buttonDot
            // 
            this.buttonDot.Location = new System.Drawing.Point(335, 334);
            this.buttonDot.Name = "buttonDot";
            this.buttonDot.Size = new System.Drawing.Size(41, 33);
            this.buttonDot.TabIndex = 102;
            this.buttonDot.Text = ".";
            this.buttonDot.UseVisualStyleBackColor = true;
            // 
            // button0
            // 
            this.button0.Location = new System.Drawing.Point(243, 334);
            this.button0.Name = "button0";
            this.button0.Size = new System.Drawing.Size(86, 33);
            this.button0.TabIndex = 100;
            this.button0.Text = "0";
            this.button0.UseVisualStyleBackColor = true;
            this.button0.Click += new System.EventHandler(this.buttons_Click);
            // 
            // buttonF
            // 
            this.buttonF.Location = new System.Drawing.Point(196, 334);
            this.buttonF.Name = "buttonF";
            this.buttonF.Size = new System.Drawing.Size(41, 33);
            this.buttonF.TabIndex = 99;
            this.buttonF.Text = "F";
            this.buttonF.UseVisualStyleBackColor = true;
            this.buttonF.Click += new System.EventHandler(this.buttons_Click);
            // 
            // buttonAnd
            // 
            this.buttonAnd.Location = new System.Drawing.Point(149, 334);
            this.buttonAnd.Name = "buttonAnd";
            this.buttonAnd.Size = new System.Drawing.Size(41, 33);
            this.buttonAnd.TabIndex = 98;
            this.buttonAnd.Text = "And";
            this.buttonAnd.UseVisualStyleBackColor = true;
            // 
            // buttonNot
            // 
            this.buttonNot.Location = new System.Drawing.Point(102, 334);
            this.buttonNot.Name = "buttonNot";
            this.buttonNot.Size = new System.Drawing.Size(41, 33);
            this.buttonNot.TabIndex = 97;
            this.buttonNot.Text = "Not";
            this.buttonNot.UseVisualStyleBackColor = true;
            // 
            // buttonEqual
            // 
            this.buttonEqual.Location = new System.Drawing.Point(429, 295);
            this.buttonEqual.Name = "buttonEqual";
            this.buttonEqual.Size = new System.Drawing.Size(41, 72);
            this.buttonEqual.TabIndex = 96;
            this.buttonEqual.Text = "=";
            this.buttonEqual.UseVisualStyleBackColor = true;
            this.buttonEqual.Click += new System.EventHandler(this.buttonEqual_Click);
            // 
            // buttonMinus
            // 
            this.buttonMinus.Location = new System.Drawing.Point(382, 295);
            this.buttonMinus.Name = "buttonMinus";
            this.buttonMinus.Size = new System.Drawing.Size(41, 33);
            this.buttonMinus.TabIndex = 95;
            this.buttonMinus.Text = "-";
            this.buttonMinus.UseVisualStyleBackColor = true;
            this.buttonMinus.Click += new System.EventHandler(this.buttonMinus_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(335, 295);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(41, 33);
            this.button3.TabIndex = 94;
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.buttons_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(288, 295);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(41, 33);
            this.button2.TabIndex = 93;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.buttons_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(243, 295);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(41, 33);
            this.button1.TabIndex = 92;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.buttons_Click);
            // 
            // buttonE
            // 
            this.buttonE.Location = new System.Drawing.Point(196, 295);
            this.buttonE.Name = "buttonE";
            this.buttonE.Size = new System.Drawing.Size(41, 33);
            this.buttonE.TabIndex = 91;
            this.buttonE.Text = "E";
            this.buttonE.UseVisualStyleBackColor = true;
            this.buttonE.Click += new System.EventHandler(this.buttons_Click);
            // 
            // buttonRsh
            // 
            this.buttonRsh.Location = new System.Drawing.Point(149, 295);
            this.buttonRsh.Name = "buttonRsh";
            this.buttonRsh.Size = new System.Drawing.Size(41, 33);
            this.buttonRsh.TabIndex = 90;
            this.buttonRsh.Text = "Rsh";
            this.buttonRsh.UseVisualStyleBackColor = true;
            // 
            // buttonLsh
            // 
            this.buttonLsh.Location = new System.Drawing.Point(102, 295);
            this.buttonLsh.Name = "buttonLsh";
            this.buttonLsh.Size = new System.Drawing.Size(41, 33);
            this.buttonLsh.TabIndex = 89;
            this.buttonLsh.Text = "Lsh";
            this.buttonLsh.UseVisualStyleBackColor = true;
            // 
            // button1x
            // 
            this.button1x.Location = new System.Drawing.Point(429, 256);
            this.button1x.Name = "button1x";
            this.button1x.Size = new System.Drawing.Size(41, 33);
            this.button1x.TabIndex = 88;
            this.button1x.Text = "1/x";
            this.button1x.UseVisualStyleBackColor = true;
            // 
            // buttonMulti
            // 
            this.buttonMulti.Location = new System.Drawing.Point(382, 256);
            this.buttonMulti.Name = "buttonMulti";
            this.buttonMulti.Size = new System.Drawing.Size(41, 33);
            this.buttonMulti.TabIndex = 87;
            this.buttonMulti.Text = "*";
            this.buttonMulti.UseVisualStyleBackColor = true;
            this.buttonMulti.Click += new System.EventHandler(this.buttonMulti_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(335, 256);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(41, 33);
            this.button6.TabIndex = 86;
            this.button6.Text = "6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.buttons_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(288, 256);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(41, 33);
            this.button5.TabIndex = 85;
            this.button5.Text = "5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.buttons_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(243, 256);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(41, 33);
            this.button4.TabIndex = 84;
            this.button4.Text = "4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.buttons_Click);
            // 
            // buttonD
            // 
            this.buttonD.Location = new System.Drawing.Point(196, 256);
            this.buttonD.Name = "buttonD";
            this.buttonD.Size = new System.Drawing.Size(41, 33);
            this.buttonD.TabIndex = 83;
            this.buttonD.Text = "D";
            this.buttonD.UseVisualStyleBackColor = true;
            this.buttonD.Click += new System.EventHandler(this.buttons_Click);
            // 
            // buttonXor
            // 
            this.buttonXor.Location = new System.Drawing.Point(149, 256);
            this.buttonXor.Name = "buttonXor";
            this.buttonXor.Size = new System.Drawing.Size(41, 33);
            this.buttonXor.TabIndex = 82;
            this.buttonXor.Text = "Xor";
            this.buttonXor.UseVisualStyleBackColor = true;
            // 
            // buttonOr
            // 
            this.buttonOr.Location = new System.Drawing.Point(102, 256);
            this.buttonOr.Name = "buttonOr";
            this.buttonOr.Size = new System.Drawing.Size(41, 33);
            this.buttonOr.TabIndex = 81;
            this.buttonOr.Text = "Or";
            this.buttonOr.UseVisualStyleBackColor = true;
            // 
            // LabelBinary
            // 
            this.LabelBinary.AutoSize = true;
            this.LabelBinary.Location = new System.Drawing.Point(12, 62);
            this.LabelBinary.Name = "LabelBinary";
            this.LabelBinary.Size = new System.Drawing.Size(0, 13);
            this.LabelBinary.TabIndex = 105;
            // 
            // operationsTextbox
            // 
            this.operationsTextbox.BackColor = System.Drawing.SystemColors.Control;
            this.operationsTextbox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.operationsTextbox.Enabled = false;
            this.operationsTextbox.Location = new System.Drawing.Point(114, 12);
            this.operationsTextbox.Name = "operationsTextbox";
            this.operationsTextbox.Size = new System.Drawing.Size(356, 13);
            this.operationsTextbox.TabIndex = 106;
            this.operationsTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // mainTextBox
            // 
            this.mainTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.mainTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mainTextBox.Enabled = false;
            this.mainTextBox.Location = new System.Drawing.Point(22, 55);
            this.mainTextBox.Name = "mainTextBox";
            this.mainTextBox.Size = new System.Drawing.Size(448, 20);
            this.mainTextBox.TabIndex = 107;
            this.mainTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.mainTextBox.TextChanged += new System.EventHandler(this.mainTextBox_TextChanged_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(485, 379);
            this.Controls.Add(this.mainTextBox);
            this.Controls.Add(this.operationsTextbox);
            this.Controls.Add(this.LabelBinary);
            this.Controls.Add(this.buttonPlus);
            this.Controls.Add(this.buttonDot);
            this.Controls.Add(this.button0);
            this.Controls.Add(this.buttonF);
            this.Controls.Add(this.buttonAnd);
            this.Controls.Add(this.buttonNot);
            this.Controls.Add(this.buttonEqual);
            this.Controls.Add(this.buttonMinus);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonE);
            this.Controls.Add(this.buttonRsh);
            this.Controls.Add(this.buttonLsh);
            this.Controls.Add(this.button1x);
            this.Controls.Add(this.buttonMulti);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.buttonD);
            this.Controls.Add(this.buttonXor);
            this.Controls.Add(this.buttonOr);
            this.Controls.Add(this.buttonPercent);
            this.Controls.Add(this.buttonDivide);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.buttonC);
            this.Controls.Add(this.buttonRoR);
            this.Controls.Add(this.buttonRoL);
            this.Controls.Add(this.buttonRoot);
            this.Controls.Add(this.buttonPM);
            this.Controls.Add(this.buttonDeleteC);
            this.Controls.Add(this.buttonDeleteCE);
            this.Controls.Add(this.buttonBackspace);
            this.Controls.Add(this.buttonB);
            this.Controls.Add(this.buttonRP);
            this.Controls.Add(this.buttonLP);
            this.Controls.Add(this.buttonMminus);
            this.Controls.Add(this.buttonMplus);
            this.Controls.Add(this.buttonMS);
            this.Controls.Add(this.buttonMR);
            this.Controls.Add(this.buttonMC);
            this.Controls.Add(this.buttonA);
            this.Controls.Add(this.buttonMod);
            this.Controls.Add(this.buttonEmpty);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.RadioButton RadioWordWord;
        public System.Windows.Forms.RadioButton RadioWordOword;
        public System.Windows.Forms.RadioButton RadioWordDword;
        public System.Windows.Forms.RadioButton RadioWordByte;
        private System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.RadioButton systemOct;
        public System.Windows.Forms.RadioButton systemHex;
        public System.Windows.Forms.RadioButton systemDec;
        public System.Windows.Forms.RadioButton systemBin;
        private System.Windows.Forms.Button buttonEmpty;
        private System.Windows.Forms.Button buttonMod;
        private System.Windows.Forms.Button buttonA;
        private System.Windows.Forms.Button buttonMC;
        private System.Windows.Forms.Button buttonMminus;
        private System.Windows.Forms.Button buttonMplus;
        private System.Windows.Forms.Button buttonMS;
        private System.Windows.Forms.Button buttonMR;
        private System.Windows.Forms.Button buttonRoot;
        private System.Windows.Forms.Button buttonPM;
        private System.Windows.Forms.Button buttonDeleteC;
        private System.Windows.Forms.Button buttonDeleteCE;
        private System.Windows.Forms.Button buttonBackspace;
        private System.Windows.Forms.Button buttonB;
        private System.Windows.Forms.Button buttonRP;
        private System.Windows.Forms.Button buttonLP;
        private System.Windows.Forms.Button buttonPercent;
        private System.Windows.Forms.Button buttonDivide;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button buttonC;
        private System.Windows.Forms.Button buttonRoR;
        private System.Windows.Forms.Button buttonRoL;
        private System.Windows.Forms.Button buttonPlus;
        private System.Windows.Forms.Button buttonDot;
        private System.Windows.Forms.Button button0;
        private System.Windows.Forms.Button buttonF;
        private System.Windows.Forms.Button buttonAnd;
        private System.Windows.Forms.Button buttonNot;
        private System.Windows.Forms.Button buttonEqual;
        private System.Windows.Forms.Button buttonMinus;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonE;
        private System.Windows.Forms.Button buttonRsh;
        private System.Windows.Forms.Button buttonLsh;
        private System.Windows.Forms.Button button1x;
        private System.Windows.Forms.Button buttonMulti;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button buttonD;
        private System.Windows.Forms.Button buttonXor;
        private System.Windows.Forms.Button buttonOr;
        public System.Windows.Forms.Label LabelBinary;
        public System.Windows.Forms.TextBox operationsTextbox;
        public System.Windows.Forms.TextBox mainTextBox;
    }
}

