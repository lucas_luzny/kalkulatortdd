﻿using System;
using kalkulator;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Windows.Forms;

namespace KalkulatorUnitTests
{
    [TestClass]
    public class UnitTest1
    {
        private Form1 form;
        private Object _object;
        private EventArgs _event;

        public UnitTest1()
        {
            this.form = new Form1();
            form.Form1_Load(_object, _event);
        }

        [TestMethod]
        public void TestStartingResult()
        {
            Assert.AreEqual("0", this.form.kalkulator.wartosc);
        }

        [TestMethod]
        public void Test0Button()
        {
            var buttonName = "button0";
            Button testedButton = Array.FindAll(form.buttons, b => b.Name.Equals(buttonName))[0];
            form.buttons_Click(testedButton, _event);
            Assert.AreEqual("0", this.form.kalkulator.wartosc);
        }

        [TestMethod]
        public void Test1Button()
        {
            var buttonName = "button1";
            Button testedButton = Array.FindAll(form.buttons, b => b.Name.Equals(buttonName))[0];
            form.buttons_Click(testedButton, _event);
            Assert.AreEqual("1", this.form.kalkulator.wartosc);
        }

        [TestMethod]
        public void Test2Button()
        {
            var buttonName = "button2";
            Button testedButton = Array.FindAll(form.buttons, b => b.Name.Equals(buttonName))[0];
            form.buttons_Click(testedButton, _event);
            Assert.AreEqual("2", this.form.kalkulator.wartosc);
        }

        [TestMethod]
        public void Test3Button()
        {
            var buttonName = "button3";
            Button testedButton = Array.FindAll(form.buttons, b => b.Name.Equals(buttonName))[0];
            form.buttons_Click(testedButton, _event);
            Assert.AreEqual("3", this.form.kalkulator.wartosc);
        }

        [TestMethod]
        public void Test4Button()
        {
            var buttonName = "button4";
            Button testedButton = Array.FindAll(form.buttons, b => b.Name.Equals(buttonName))[0];
            form.buttons_Click(testedButton, _event);
            Assert.AreEqual("4", this.form.kalkulator.wartosc);
        }

        [TestMethod]
        public void Test5Button()
        {
            var buttonName = "button5";
            Button testedButton = Array.FindAll(form.buttons, b => b.Name.Equals(buttonName))[0];
            form.buttons_Click(testedButton, _event);
            Assert.AreEqual("5", this.form.kalkulator.wartosc);
        }

        [TestMethod]
        public void Test6Button()
        {
            var buttonName = "button6";
            Button testedButton = Array.FindAll(form.buttons, b => b.Name.Equals(buttonName))[0];
            form.buttons_Click(testedButton, _event);
            Assert.AreEqual("6", this.form.kalkulator.wartosc);
        }

        [TestMethod]
        public void Test7Button()
        {
            var buttonName = "button7";
            Button testedButton = Array.FindAll(form.buttons, b => b.Name.Equals(buttonName))[0];
            form.buttons_Click(testedButton, _event);
            Assert.AreEqual("7", this.form.kalkulator.wartosc);
        }

        [TestMethod]
        public void Test8Button()
        {
            var buttonName = "button8";
            Button testedButton = Array.FindAll(form.buttons, b => b.Name.Equals(buttonName))[0];
            form.buttons_Click(testedButton, _event);
            Assert.AreEqual("8", this.form.kalkulator.wartosc);
        }

        [TestMethod]
        public void Test9Button()
        {
            var buttonName = "button9";
            Button testedButton = Array.FindAll(form.buttons, b => b.Name.Equals(buttonName))[0];
            form.buttons_Click(testedButton, _event);
            Assert.AreEqual("9", this.form.kalkulator.wartosc);
        }

        [TestMethod]
        public void TestAButton()
        {
            var buttonName = "buttonA";
            Button testedButton = Array.FindAll(form.buttons, b => b.Name.Equals(buttonName))[0];
            form.buttons_Click(testedButton, _event);
            Assert.AreEqual("A", this.form.kalkulator.wartosc);
        }

        [TestMethod]
        public void TestBButton()
        {
            var buttonName = "buttonB";
            Button testedButton = Array.FindAll(form.buttons, b => b.Name.Equals(buttonName))[0];
            form.buttons_Click(testedButton, _event);
            Assert.AreEqual("B", this.form.kalkulator.wartosc);
        }

        [TestMethod]
        public void TestCButton()
        {
            var buttonName = "buttonC";
            Button testedButton = Array.FindAll(form.buttons, b => b.Name.Equals(buttonName))[0];
            form.buttons_Click(testedButton, _event);
            Assert.AreEqual("C", this.form.kalkulator.wartosc);
        }

        [TestMethod]
        public void TestDButton()
        {
            var buttonName = "buttonD";
            Button testedButton = Array.FindAll(form.buttons, b => b.Name.Equals(buttonName))[0];
            form.buttons_Click(testedButton, _event);
            Assert.AreEqual("D", this.form.kalkulator.wartosc);
        }

        [TestMethod]
        public void TestEButton()
        {
            var buttonName = "buttonE";
            Button testedButton = Array.FindAll(form.buttons, b => b.Name.Equals(buttonName))[0];
            form.buttons_Click(testedButton, _event);
            Assert.AreEqual("E", this.form.kalkulator.wartosc);
        }

        [TestMethod]
        public void TestFButton()
        {
            var buttonName = "buttonF";
            Button testedButton = Array.FindAll(form.buttons, b => b.Name.Equals(buttonName))[0];       
            form.buttons_Click(testedButton, _event);
            Assert.AreEqual("F", this.form.kalkulator.wartosc);
        }

        [TestMethod]
        public void TestAdditionButton()
        {
            Button button6= Array.FindAll(form.buttons, b => b.Name.Equals("button6"))[0];
            Button button2 = Array.FindAll(form.buttons, b => b.Name.Equals("button2"))[0];
            Button buttonPlus = Array.FindAll(form.buttons, b => b.Name.Equals("buttonPlus"))[0];
            Button buttonEqual = Array.FindAll(form.buttons, b => b.Name.Equals("buttonEqual"))[0];

            form.buttons_Click(button6, _event); 
            form.buttonPlus_Click(buttonPlus, _event);
            form.buttons_Click(button2, _event);
            form.buttonEqual_Click(buttonEqual, _event);

            Assert.AreEqual("8", this.form.mainTextBox.Text);
        }

        [TestMethod]
        public void TestSubstractionButton()
        {
            Button button6 = Array.FindAll(form.buttons, b => b.Name.Equals("button6"))[0];
            Button button2 = Array.FindAll(form.buttons, b => b.Name.Equals("button2"))[0];
            Button buttonPlus = Array.FindAll(form.buttons, b => b.Name.Equals("buttonPlus"))[0];
            Button buttonEqual = Array.FindAll(form.buttons, b => b.Name.Equals("buttonEqual"))[0];

            form.buttons_Click(button6, _event);
            form.buttonMinus_Click(buttonPlus, _event);
            form.buttons_Click(button2, _event);
            form.buttonEqual_Click(buttonEqual, _event);

            Assert.AreEqual("4", this.form.mainTextBox.Text);
        }

        [TestMethod]
        public void TestMultiplicationButton()
        {
            Button button6 = Array.FindAll(form.buttons, b => b.Name.Equals("button6"))[0];
            Button button2 = Array.FindAll(form.buttons, b => b.Name.Equals("button2"))[0];
            Button buttonPlus = Array.FindAll(form.buttons, b => b.Name.Equals("buttonPlus"))[0];
            Button buttonEqual = Array.FindAll(form.buttons, b => b.Name.Equals("buttonEqual"))[0];

            form.buttons_Click(button6, _event);
            form.buttonMulti_Click(buttonPlus, _event);
            form.buttons_Click(button2, _event);
            form.buttonEqual_Click(buttonEqual, _event);

            Assert.AreEqual("12", this.form.mainTextBox.Text);
        }

        [TestMethod]
        public void TestDivisonButton()
        {
            Button button6 = Array.FindAll(form.buttons, b => b.Name.Equals("button6"))[0];
            Button button2 = Array.FindAll(form.buttons, b => b.Name.Equals("button2"))[0];
            Button buttonPlus = Array.FindAll(form.buttons, b => b.Name.Equals("buttonPlus"))[0];
            Button buttonEqual = Array.FindAll(form.buttons, b => b.Name.Equals("buttonEqual"))[0];

            form.buttons_Click(button6, _event);
            form.buttonDivide_Click(buttonPlus, _event);
            form.buttons_Click(button2, _event);
            form.buttonEqual_Click(buttonEqual, _event);

            Assert.AreEqual("3", this.form.mainTextBox.Text);
        }

        [TestMethod]
        public void TestBackspaceButton()
        {
            Button button6 = Array.FindAll(form.buttons, b => b.Name.Equals("button6"))[0];
            Button buttonBackspace = Array.FindAll(form.buttons, b => b.Name.Equals("buttonBackspace"))[0];

            form.buttons_Click(button6, _event);
            form.buttonBackspace_Click(buttonBackspace, _event);

            Assert.AreEqual("0", this.form.mainTextBox.Text);
        }

        [TestMethod]
        public void TestDeleteCEButton()
        {
            Button button6 = Array.FindAll(form.buttons, b => b.Name.Equals("button6"))[0];
            Button buttonDeleteCE = Array.FindAll(form.buttons, b => b.Name.Equals("buttonDeleteCE"))[0];

            form.buttons_Click(button6, _event);
            form.buttonDeleteCE_Click(buttonDeleteCE, _event);

            Assert.AreEqual("0", this.form.mainTextBox.Text);
        }

        [TestMethod]
        public void TestDeleteCButton()
        {
            Button button6 = Array.FindAll(form.buttons, b => b.Name.Equals("button6"))[0];
            Button buttonDeleteC = Array.FindAll(form.buttons, b => b.Name.Equals("buttonDeleteC"))[0];

            form.buttons_Click(button6, _event);
            form.buttonDeleteC_Click(buttonDeleteC, _event);

            Assert.AreEqual("0", this.form.mainTextBox.Text);
        }

        [TestMethod]
        public void TestPlusMinusButton()
        {
            Button button6 = Array.FindAll(form.buttons, b => b.Name.Equals("button6"))[0];
            Button buttonPM = Array.FindAll(form.buttons, b => b.Name.Equals("buttonPM"))[0];

            form.buttons_Click(button6, _event);
            form.buttonPM_Click(buttonPM, _event);

            Assert.AreEqual("-6", this.form.mainTextBox.Text);
        }

        [TestMethod]
        public void TestDefaultNumericalSystem()
        {
            foreach (var button in form.hexDisabled)
            {
                Assert.IsFalse(button.Enabled);
            }
        }

        [TestMethod]
        public void TestHexButton()
        {
            RadioButton hexButton = Array.FindAll(form.rButtons, b => b.Name.Equals("systemHex"))[0];
            form.systemHex_CheckedChanged(hexButton, _event);

            foreach (var button in form.hexDisabled)
            {
                Assert.IsFalse(button.Enabled);
            }
        }

        [TestMethod]
        public void TestDecButton()
        {
            RadioButton decButton = Array.FindAll(form.rButtons, b => b.Name.Equals("systemDec"))[0];
            form.systemDec_CheckedChanged(decButton, _event);

            foreach (var button in form.decDisabled)
            {
                Assert.IsFalse(button.Enabled);
            }
        }

        [TestMethod]
        public void TestOctButton()
        {
            RadioButton octButton = Array.FindAll(form.rButtons, b => b.Name.Equals("systemOct"))[0];
            form.systemOct_CheckedChanged(octButton, _event);

            foreach (var button in form.octDisabled)
            {
                Assert.IsFalse(button.Enabled);
            }
        }

        [TestMethod]
        public void TestBinButton()
        {
            RadioButton binButton = Array.FindAll(form.rButtons, b => b.Name.Equals("systemBin"))[0];
            form.systemBin_CheckedChanged(binButton, _event);

            foreach (var button in form.binDisabled)
            {
                Assert.IsFalse(button.Enabled);
            }
        }

    }
}
